#
# Cookbook Name:: mongodb
# resource:: mms
#
# Copyright 2012, Robert Allen, @zircote
#
#

actions :install, :uninstall
default_action :install

attribute :mms_key, :name_attribute => true, :kind_of => String
attribute :secret_key, :required => true, :kind_of => String
attribute :install_dir, :required => true, :kind_of => String
attribute :log_dir, :required => true, :kind_of => String
attribute :user, :required => true, :kind_of => String
attribute :group, :required => true, :kind_of => String


