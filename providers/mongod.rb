#
# Cookbook Name:: mongodb
# provider:: mongod
#
# Copyright 2012, Robert Allen, @zircote
#
#
action :install do

  service "mongodb" do
    action [:stop, :disable]
  end

  service "#{new_resource.svc_name}" do
    action :nothing
    supports :stop => true, :start => true, :restart => true, :reload => true
  end

  template "#{new_resource.svc_name}" do
    owner "root"
    group "root"
    mode 0755
    path "/etc/init.d/#{new_resource.svc_name}"
    case node.platform
      when "redhat", "centos", "scientific", "fedora", "suse", "amazon"
        source new_resource.type == 'mongos' ? "mongos.rh.init.erb" : "mongod.rh.init.erb"
        if new_resource.type != 'mongos' && new_resource.dbpath.nil?
          raise "dbpath must be defined"
        end
      when "ubuntu", "debian"
        source new_resource.type == "mongos" ? "mongos.init.erb" : "mongod.init.erb"
      else
        raise('unsupported distribution')
    end
    variables ({:svc_name => new_resource.svc_name, :dbpath => new_resource.dbpath})
    action :create
    notifies :restart, {:service => "#{new_resource.svc_name}"}, :delayed
  end

  template "#{new_resource.config}" do
    source "#{new_resource.type}.conf.erb"
    owner "root"
    group "root"
    mode 0644
    action :create
    notifies :restart, {:service => "#{new_resource.svc_name}"}, :delayed
  end

  if new_resource.type != 'mongos'
    directory "#{new_resource.dbpath}" do
      user new_resource.user
      group new_resource.group
      action :create
      recursive true
    end
  end

  directory "#{new_resource.log_dir}" do
    user new_resource.user
    group new_resource.group
    action :create
    recursive true
  end

  file "#{new_resource.logpath}" do
    mode 0644
    user new_resource.user
    group new_resource.group
    action :create_if_missing
  end

  if %w(redhat centos scientific fedora suse amazon).include? node.platform
    file "/etc/sysconfig/#{new_resource.svc_name}" do
      owner "root"
      group "root"
      mode 0644
      action :create_if_missing
      notifies :restart, {:service => "#{new_resource.svc_name}"}, :delayed
    end
  end

  service "#{new_resource.svc_name}" do
    action :enable
  end
end
