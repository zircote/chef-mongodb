provides "mongodb"
require_plugin 'ruby'

def recurse(hash)
  result = Mash.new
  hash.each do |k, v|
    if v.is_a? BSON::OrderedHash
      result[k] = recurse(v)
    elsif v.is_a? Hash
      result[k] = recurse(v)
    elsif v.is_a? Array
      result[k] = recurse(v)
    else
      result[k] = v.to_s
    end
  end
  result
end

def group
  { :mongod => 27018, :mongoc => 27019}
end

%w(rubygems json bson orderedhash mongo ).each do |g|
  begin
    require g
  rescue Exception => e
    nil
  end
end

mongodb Mash.new unless mongodb
mongodb[:status] = Mash.new
group.each do |k, v|
  begin
    mongodb[:status][k] = Mash.new
    c = Mongo::Connection.new("localhost", v.to_s, :safe => true)
    dbstats = OrderedHash.new
    dbstats.push(:dbstats, 1)
    dbstats = c.db('admin').command(dbstats)
    dbstats = dbstats.to_hash
    mongodb[:status][k][:dbstats] = Mash.new
    mongodb[:status][k][:dbstats] = dbstats
  rescue Mongo::ConnectionFailure
    next
  rescue Exception => e
    mongodb[:status][k][:dbstats] = e.to_s
    nil
  end
  begin
    server_status = OrderedHash.new
    server_status.push(:serverStatus, 1)
    server_status = c.db('admin').command(server_status)
    server_status = recurse(server_status.to_hash)
    mongodb[:status][k][:server_status] = Mash.new
    mongodb[:status][k][:server_status] = server_status
  rescue Exception => e
    mongodb[:status][k][:server_status] = e.to_s
  end
  begin
    replset_status = OrderedHash.new
    replset_status.push(:replSetGetStatus, 1)
    replset_status = c.db('admin').command(replset_status)
    replset_status = recurse(replset_status.to_hash)
    mongodb[:status][k][:replset_status] = Mash.new
    mongodb[:status][k][:replset_status] = replset_status
  rescue Exception => e
    mongodb[:status][k][:replset_status] = e.to_s
  end
end
