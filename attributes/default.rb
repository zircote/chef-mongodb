#
# Cookbook Name:: mongodb
# attributes:: default
#
# Copyright 2012, Robert Allen, @zircote
#
#

case node.platform
  when "redhat","centos","scientific","fedora","suse","amazon"
    user = 'mongod'
    default[:mongodb][:package] = "mongo-10gen mongo-10gen-server"
  else
    user = 'mongodb'
    default[:mongodb][:package] = "mongodb-10gen"
end

default[:mongodb][:user] = user
default[:mongodb][:group] = user
default[:users ][user][:uid] = 460
default[:groups][user][:gid] = 460

### GENERAL
default[:mongodb][:svc_name] = "mongod"
default[:mongodb][:dbpath] = "/var/db/#{node[:mongodb][:svc_name]}"
default[:mongodb][:config] = "/etc/#{node[:mongodb][:svc_name]}.conf"
default[:mongodb][:init_system] = "sysv"
default[:mongodb][:log_dir] = "/var/log"
default[:mongodb][:logpath] = "#{node[:mongodb][:log_dir]}/#{node[:mongodb][:svc_name]}.log"
default[:mongodb][:logappend] = true
default[:mongodb][:port] = 27017
default[:mongodb][:bind_ip] = "0.0.0.0"

# Authentication
default[:mongodb][:auth][:enabled]  = false
default[:mongodb][:auth][:username] = nil
default[:mongodb][:auth][:password] = nil

### EXTRA
default[:mongodb][:nojournal] = false
default[:mongodb][:cpu] = false
default[:mongodb][:verbose] = false
default[:mongodb][:objcheck] = false
default[:mongodb][:quota] = false
default[:mongodb][:diaglog] = false
default[:mongodb][:nocursors] = false
default[:mongodb][:nohints] = false
default[:mongodb][:nohttpinterface] = false
default[:mongodb][:rest_enabled] = false
default[:mongodb][:noscripting] = false
default[:mongodb][:notablescan] = false
default[:mongodb][:noprealloc] = false
default[:mongodb][:nssize] = false

# ReplicaSet
default[:mongodb][:replica_set][:name] = false
# oplog is typically 5% of available disk space
# Once the mongod has created the oplog for the first time, changing oplogSize will not affect the size of the oplog.
default[:mongodb][:replica_set][:oplogSize] = false
default[:mongodb][:replica_set][:fastsync] = false
# replIndexPrefetch version>=2.2
# Specify _id_only or none to prevent the mongod from loading any index into memory.
# all, none, and _id_only
if /mongodb-10gen/.match(node[:mongodb][:package])
  default[:mongodb][:replica_set][:replIndexPrefetch] = nil
end
default[:mongodb][:replica_set][:members] = []
default[:mongodb][:replica_set][:host] = "#{node[:fqdn]}:#{node[:mongodb][:port]}"
default[:mongodb][:replica_set][:arbiter_only] = false
default[:mongodb][:replica_set][:build_indexes] = false
default[:mongodb][:replica_set][:hidden] = false
default[:mongodb][:replica_set][:priority] = nil
default[:mongodb][:replica_set][:tags] = []
default[:mongodb][:replica_set][:slave_delay] = 0
default[:mongodb][:replica_set][:votes] = 1

### SHARDING
default[:mongodb][:shard_server] = nil

### STARTUP
default[:mongodb][:rest] = false
default[:mongodb][:syncdelay] = 60

### REPLICATION
default[:mongodb][:replication] = false
default[:mongodb][:slave] = false
default[:mongodb][:slave_source] = ""
default[:mongodb][:slave_only] = ""
default[:mongodb][:master] = false
default[:mongodb][:autoresync] = false
default[:mongodb][:oplogsize] = 0
default[:mongodb][:opidmem] = 0

### CONFIG SERVER
default[:mongodb][:mongoc][:svc_name] = "mongoc"
default[:mongodb][:mongoc][:dbpath] = "/var/db/#{default[:mongodb][:mongoc][:svc_name]}"
default[:mongodb][:mongoc][:logpath] = "#{default[:mongodb][:log_dir]}/#{default[:mongodb][:mongoc][:svc_name]}.log"
default[:mongodb][:mongoc][:logappend] = true
default[:mongodb][:mongoc][:config] = "/etc/#{node[:mongodb][:mongoc][:svc_name]}.conf"
default[:mongodb][:mongoc][:host] = node['fqdn']
default[:mongodb][:mongoc][:port] = 27019

### MONGOS
default[:mongodb][:mongos][:svc_name] = "mongos"
default[:mongodb][:mongos][:pidfilepath] = "#{node[:mongodb][:dbpath]}/#{node[:mongodb][:mongos][:svc_name]}.lock"
default[:mongodb][:mongos][:config] = "/etc/#{node[:mongodb][:mongos][:svc_name]}.conf"
default[:mongodb][:mongos][:logpath] = "#{node[:mongodb][:log_dir]}/#{node[:mongodb][:mongos][:svc_name]}.log"
default[:mongodb][:mongos][:logappend] = true
default[:mongodb][:mongos][:host] = "localhost"
default[:mongodb][:mongos][:port] = 27017
default[:mongodb][:mongos][:configdb] = "localhost:27019"

# MMS
default[:mongodb][:mms][:svc_name] = "mms-agent"
default[:mongodb][:mms][:mms_key] = ''
default[:mongodb][:mms][:secret_key] = ''
default[:mongodb][:mms][:log_dir] = node[:mongodb][:log_dir]
default[:mongodb][:mms][:logpath] = "#{node[:mongodb][:mms][:log_dir]}/#{node[:mongodb][:mms][:svc_name]}.log"
default[:mongodb][:mms][:install_dir] = "/opt/#{node[:mongodb][:mms][:svc_name]}"
default[:mongodb][:mms][:user] = node[:mongodb][:user]
default[:mongodb][:mms][:group] = node[:mongodb][:user]

default[:mongodb][:logrotate][:logpath] = "#{node[:mongodb][:log_dir]}/mongo*.log"
default[:mongodb][:logrotate][:frequency] = "daily"
default[:mongodb][:logrotate][:missingok] = true
default[:mongodb][:logrotate][:notifempty] = true
default[:mongodb][:logrotate][:copytruncate] = true
default[:mongodb][:logrotate][:rotate] = 7
default[:mongodb][:logrotate][:compress] = true
default[:mongodb][:logrotate][:compress_cmd] = "/usr/bin/bzip2"
default[:mongodb][:logrotate][:uncompress_cmd] = "/usr/bin/bunzip2"
default[:mongodb][:logrotate][:compress_ext] = ".bz2"
default[:mongodb][:logrotate][:dateext] = true



### BACKUP
default[:mongodb][:backup][:host] = "localhost"
default[:mongodb][:backup][:backupdir] = "/var/backups/mongodb"
