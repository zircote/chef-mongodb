#
# Cookbook Name:: mongodb
# provider:: mms
#
# Copyright 2012, Robert Allen, @zircote
#
#

action :install do
  remote_file "#{Chef::Config[:file_cache_path]}/10gen-mms-agent.tar.gz" do
    source "https://mms.10gen.com/settings/10gen-mms-agent.tar.gz"
    action :create_if_missing
  end

  bash "install-mms-agent" do
    cwd Chef::Config[:file_cache_path]
    code <<-EOF
      tar xzf 10gen-mms-agent.tar.gz
      cd mms-agent
      sed -e 's/@API_KEY@/#{new_resource.mms_key}/' -i '' settings.py
      sed -e 's/@SECRET_KEY@/#{new_resource.secret_key}/' -i '' settings.py
      mkdir -p #{new_resource.install_dir}
      cd -
      mv mms-agent/* #{new_resource.install_dir}/
      chown -R #{new_resource.user}:#{new_resource.group} #{new_resource.install_dir}
    EOF
    action :run
  end

  template "/etc/init.d/mms-agent" do
    mode '0755'
    owner 'root'
    group 'root'
    case node.platform
      when "redhat","centos","scientific","fedora","suse","amazon"
        source "mms-agent.rh.init.erb"
      when "ubuntu", "debian"
        source "mms-agent.init.erb"
      else
        raise('unsupported platform')
    end
    variables ({
        :install_dir => new_resource.install_dir,
        :log_dir => new_resource.log_dir,
        :user => new_resource.user
    })
  end

end

action :uninstall do
  service "mms-agent" do
    supports [:start => true, :stop => true, :restart => true]
    action [:disable, :stop]
  end

  file "/etc/init.d/mms-agent" do
    action :delete
  end

  file "#{Chef::Config[:file_cache_path]}/10gen-mms-agent.tar.gz" do
    action :delete
  end

  file new_resource.install_dir do
    action :delete
  end
end

