#
# Cookbook Name:: mongodb
# Recipe:: default
#
# Copyright 2012, Robert Allen, @zircote
#
#

template "/etc/logrotate.d/mongodb" do
  source "logrotate.mongodb.erb"
  variables ({
      :logrotate => node[:mongodb][:logrotate],
  })
  action :create
end
