#
# Cookbook Name:: mongodb
# Recipe:: default
#
# Copyright 2012, Robert Allen, @zircote
#
#

include_recipe "mongodb::10gen_repo"

group "#{node[:mongodb][:group]}" do
  gid node[:groups][node[:mongodb][:user]][:gid]
  system true
end

user "#{node[:mongodb][:user]}" do
  uid node[:users][node[:mongodb][:user]][:uid]
  gid node[:groups][node[:mongodb][:user]][:gid]
  home "/var/lib/mongo"
  comment "mongodb system user"
  system true
end

node[:mongodb][:package].split(' ').each do |p|
  package "#{p}" do
    action :install
  end
end

