#
# Cookbook Name:: mongodb
# Recipe:: mongod
#
# Copyright 2012, Robert Allen, @zircote
#
#

include_recipe "mongodb::default"

mongodb_mongod "#{node[:mongodb][:svc_name]}" do
  config  node[:mongodb][:config]
  dbpath  node[:mongodb][:dbpath]
  log_dir node[:mongodb][:log_dir]
  logpath node[:mongodb][:logpath]
  user    node[:mongodb][:user]
  group   node[:mongodb][:group]
end
