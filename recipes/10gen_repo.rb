#
# Cookbook Name:: mongodb
# Recipe:: 10gen_repo
#
# Copyright 2012, Robert Allen, @zircote
#
#

case node['platform']
  when "debian", "ubuntu"
    execute "apt-get update" do
      action :nothing
    end
    apt_repository "10gen" do
      uri "http://downloads-distro.mongodb.org/repo/debian-sysvinit"
      distribution "dist"
      components ["10gen"]
      keyserver "hkp://keyserver.ubuntu.com:80"
      key "7F0CEB10"
      action :add
      notifies :run, "execute[apt-get update]", :immediately
    end
  when "centos", "redhat", "fedora", "amazon"
    yum_repository "10gen" do
      description "10gen RPM Repository"
      url "http://downloads-distro.mongodb.org/repo/redhat/os/#{node['kernel']['machine'] =~ /x86_64/ ? 'x86_64' : 'i686'}"
      action :add
    end
end
