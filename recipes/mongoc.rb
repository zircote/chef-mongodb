#
# Cookbook Name:: mongodb
# Recipe:: mongoc
#
# Copyright 2012, Robert Allen, @zircote
#
#

include_recipe "mongodb::default"

mongodb_mongod "#{node[:mongodb][:mongoc][:svc_name]}" do
  config  node[:mongodb][:mongoc][:config]
  dbpath  node[:mongodb][:mongoc][:dbpath]
  log_dir node[:mongodb][:log_dir]
  logpath node[:mongodb][:mongoc][:logpath]
  user    node[:mongodb][:user]
  group   node[:mongodb][:group]
  type 'mongoc'
end
