#
# Cookbook Name:: mongodb
# Recipe:: mms_agent
#
# Copyright 2012, Robert Allen, @zircote
#
#

include_recipe "build-essential::default"
include_recipe "python::pip"


python_pip "pymongo" do
  action :install
end

mongodb_mms "#{node[:mongodb]['mms']['mms_key']}" do
  secret_key  node[:mongodb]['mms']['secret_key']
  install_dir node[:mongodb]['mms']['install_dir']
  log_dir     node[:mongodb]['mms']['log_dir']
  user        node[:mongodb]['user']
  group       node[:mongodb]['group']
end

service "mms-agent" do
  supports [:start => true, :stop => true, :restart => true]
  action [:enable, :start]
end

