#
# Cookbook Name:: mongodb
# resource:: mongod
#
# Copyright 2012, Robert Allen, @zircote
#
#

actions :install
default_action :install

attribute :svc_name, :name_attribute => true, :kind_of => String
attribute :config, :required => true, :kind_of => String
attribute :dbpath, :default => nil, :kind_of => String
attribute :log_dir, :required => true, :kind_of => String
attribute :logpath, :required => true, :kind_of => String
attribute :user, :required => true, :kind_of => String
attribute :group, :required => true, :kind_of => String
attribute :type, :default => 'mongod', :kind_of => String, :regex => /^(mongos|mongod|mongoc)$/


