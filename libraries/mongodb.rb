#
#
#
#
#
#
#

class MongoDB
  # @var [Mongo::Connection]
  @mongo = nil
  #
  def initialize(host = 'localhost', port = 27017)
    require "json"
    require 'orderedhash'
    require 'mongo'
    if host.is_a?(Mongo::Connection)
      @mongo = host
    else
      @mongo = Mongo::Connection.new(host, port, :safe => true)
    end
    @host = host
    @port = port
    self
  end

  #
  #
  # @return [Hash]
  def replset_status(rs_name)
    raise('invalid type for replset name') unless rs_name.is_a?(String)
    local = @mongo.db('local')
    local['system.replset'].find_one({:_id => rs_name})
  end

  #
  #
  # @return [Hash]|[String]
  def replset_init()
    cmd = OrderedHash.new
    cmd.push(:replSetInitiate, 1)
    admin = @mongo.db('admin')
    begin
      admin.command(cmd)
    rescue Mongo::OperationFailure => error
      raise error unless /already initialized/.match(error.message)
    end
    true
  end
end

