#
# Cookbook Name:: mongodb
# Recipe:: mongos
#
# Copyright 2012, Robert Allen, @zircote
#
#

include_recipe "mongodb::default"

mongodb_mongod "#{node[:mongodb][:mongos][:svc_name]}" do
  config  node[:mongodb][:mongos][:config]
  log_dir node[:mongodb][:log_dir]
  logpath node[:mongodb][:mongos][:logpath]
  user    node[:mongodb][:user]
  group   node[:mongodb][:group]
  type 'mongos'
end
